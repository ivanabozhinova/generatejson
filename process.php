<?php
$data = $_POST;
$data['keynote_speakers'] = explode(", ", $data['keynote_speakers']);
$data['previous_conferences'] = explode(", ", $data['previous_conferences']);


$important_dates = array(
    'Conference dates' => $data['start_date']. "  - " .  $data['end_date'],
    'Submission of papers' => $data['submission_of_papers'],
    'Notification of acceptance' => $data['notification_of_acceptance'],
    'Camera-ready paper' => $data['camera-ready_paper']
);
unset($data['submission_of_papers']);
unset($data['notification_of_acceptance']);
unset($data['camera-ready_paper']);
$data['important_dates'] = $important_dates;


$organizers = array(
    'name' => $data['organizer_name'],
    'description' => $data['organizer_description']
);
unset($data['organizer_name']);
unset($data['organizer_description']);
$data['organizers'] = $organizers;


$contact = array(
    'name' => $data['contact_name'],
    'phone' => $data['contact_phone'],
    'email' => $data['contact_email']
);
unset($data['contact_name']);
unset($data['contact_phone']);
unset($data['contact_email']);
$data['contact'] = $contact;


$json_output = json_encode($data);
//cho $json_output;
$fp = fopen('results.json', 'w');
fwrite($fp, $json_output);
fclose($fp);

