<html>
<head>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
</head>
<body>
<div class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ home_link }}">
                <i class="glyphicon glyphicon-calendar"></i>
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">

                <li><a href="{{ path('dashboard') }}"><i class="glyphicon glyphicon-th-list"></i> Dashboard</a></li>
                <li><a href="{{ path('preferences') }}"><i class="glyphicon glyphicon-cog"></i> Preferences</a></li>
                <li><a href="{{ path('login') }}"><i class="glyphicon glyphicon-user"></i> Login</a></li>
                <li><a href="{{ path('register') }}"><i class="glyphicon glyphicon-user"></i> Register</a></li>

            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
<div class="" style=" float: none;  margin: 0 auto; width: 800px">
    <form class="form-horizontal" role="form" method="post" action="process.php">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">Basic information</h5>
            </div>
            </br>
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Conference name:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name">
                </div>

            </div>
            <div class="form-group">
                <label for="about" class="col-sm-3 control-label">Conference description:</label>

                <div class="col-sm-8">
                    <textarea class="form-control" id="about" name="about"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="city" class="col-sm-3 control-label">City:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="city" name="city">
                </div>
            </div>
            <div class="form-group">
                <label for="country" class="col-sm-3 control-label">Country:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="country" name="country">
                </div>
            </div>
            <div class="form-group">
                <label for="keynote_speakers" class="col-sm-3 control-label">Keynote speakers:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="keynote_speakers" name="keynote_speakers">
                </div>
            </div>
            <div class="form-group">

                <label for="about_city" class="col-sm-3 control-label">About the city:</label>

                <div class="col-sm-8">
                    <textarea type="text" class="form-control" id="about_city" name="about_city"></textarea>
                </div>
            </div>
            <div class="form-group">

                <label for="address" class="col-sm-3 control-label">Address:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="address" name="address">
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>
            </div>
            <div class="form-group">
                <label for="previous_conferences" class="col-sm-3 control-label">Previous conferences:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="previous_conferences" name="previous_conferences">
                </div>
            </div>
            <div class="form-group">
                <label for="images" class="col-sm-3 control-label">Images:</label>

                <div class="col-sm-8">
                    <input type="file" class="form-control" id="images" name="images">
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class=" panel-title">Important dates</h5>
            </div>
            </br>
            <div class="form-group">
                <label for="start_date" class="col-sm-3 control-label">Start date:</label>

                <div class="col-sm-8">
                    <input type="date" class="form-control" id="start_date" name="start_date">
                </div>
            </div>
            <div class="form-group">
                <label for="end_date" class="col-sm-3 control-label">End date:</label>

                <div class="col-sm-8">
                    <input type="date" class="form-control" id="end_date" name="end_date">
                </div>
            </div>
            <div class="form-group">
                <label for="submission_of_papers" class="col-sm-3 control-label">Submission of papers:</label>

                <div class="col-sm-8">
                    <input type="date" class="form-control" id="submission_of_papers" name="submission_of_papers">
                </div>
            </div>
            <div class="form-group">
                <label for="notification_of_acceptance" class="col-sm-3 control-label">Notification of
                    acceptance:</label>

                <div class="col-sm-8">
                    <input type="date" class="form-control" id="notification_of_acceptance"
                           name="notification_of_acceptance">
                </div>
            </div>
            <div class="form-group">
                <label for="camera-ready_paper" class="col-sm-3 control-label">Camera-ready paper:</label>

                <div class="col-sm-8">
                    <input type="date" class="form-control" id="camera-ready_paper" name="camera-ready_paper">
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class=" panel-title">Organizers</h5>
            </div>
            </br>

            <div class="form-group">

                <label for="organizer_name" class="col-sm-3 control-label">Name:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="organizer_name" name="organizer_name">
                </div>
            </div>
            <div class="form-group">

                <label for="organizer_description" class="col-sm-3 control-label">Description:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="organizer_description" name="organizer_description">
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class=" panel-title">Contact</h5>
            </div>
            </br>

            <div class="form-group">
                <label for="contact_name" class="col-sm-3 control-label">Name:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="contact_name" name="contact_name">
                </div>
            </div>
            <div class="form-group">
                <label for="contact_email" class="col-sm-3 control-label">E-mail:</label>

                <div class="col-sm-8">
                    <input type="email" class="form-control" id="contact_email" name="contact_email">
                </div>
            </div>
            <div class="form-group">
                <label for="contact_phone" class="col-sm-3 control-label">Phone:</label>

                <div class="col-sm-8">
                    <input type="text" class="form-control" id="contact_phone" name="contact_phone">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="programme_link" class="col-sm-3 control-label">Programme link:</label>

            <div class="col-sm-8">
                <input type="text" class="form-control" id="programme_link" name="programme_link">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>